package main

import (
	"fmt"
	tg "gopkg.in/telegram-bot-api.v4"
	"net"
	"os"
	"time"
)

func timeTXT() string {
	thisTime := time.Now().UTC().Add(7 * time.Hour)
	return fmt.Sprintf("%d-%02d-%02d  %02d:%02d:%02d", thisTime.Year(), int(thisTime.Month()), thisTime.Day(), thisTime.Hour(), thisTime.Minute(), thisTime.Second())
}

func IPignore() bool {
	if len(os.Args) <= 6 {
		return false
	}

	remoteIP := net.ParseIP(os.Args[1])

	for i := 6; i < len(os.Args); i++ {
		_, netTest, err := net.ParseCIDR(os.Args[i])
		if err != nil {
			chkIP := net.ParseIP(os.Args[i])
			if chkIP == nil {
				continue
			}
			if remoteIP.Equal(chkIP) {
				return true
			}
			continue
		}
		if netTest.Contains(remoteIP) {
			return true
		}
	}
	return false
}

func main() {
	if IPignore() {
		return
	}

	bot, err := tg.NewBotAPI(token)
	if err != nil {
		return
	}
	remoteIP := os.Args[1]
	serverIP := os.Args[3]
	userName := os.Getenv("USER")
	hostName := os.Args[5]
	textSend := fmt.Sprintf("%s\nHost : %s (%s)\nRemote : %s\nUser : %s", timeTXT(), serverIP, hostName, remoteIP, userName)

	for k := range id {
		msg := tg.NewMessage(id[k], textSend)
		bot.Send(msg)
	}
}
